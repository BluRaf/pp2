#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 10

int main()
{
  int a[MAX][MAX];
  int x, y, f; /* współrzędne skrzyżowania i wynik scanf */
  do {
    printf("Wprowadz x, y: ");
    f = scanf("%i%i", &x, &y);
    fflush(stdin);
    printf("Pobrano %i liczby\n", f);
  } while(f != 2 || x < 1 || x > MAX || y < 1 || y > MAX);
    /* pobrano dwie liczby? mieszczą się w granicach tablicy? */

  srand(time(NULL));

  /* człowiek indeksuje od 1, komputer od 0 */
  x -= 1;
  y -= 1;

  int m, n;
  int suma = 0;
  /* n -> x (poziomo), m -> y (pionowo) */
  for (n = 0; n < MAX; n++) {
    for (m = 0; m < MAX; m++) {
      a[m][n] = rand()%10 + 1;
      printf("%2i ", a[m][n]);
      if ((m-n==x-y) || (m+n==x+y)) {
        suma += a[m][n];
      }
    }
    printf("\n");
  }

  printf("%i\n", suma);

  return 0;
}

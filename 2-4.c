#include <stdio.h>

/* x == NULL -> *x == 0 */

int ptradd(int *result, int *a, int *b)
{
  if (a == NULL && b == NULL) return 0;
  if (result != NULL) {
    *result = *a + *b;
    if (b == NULL) *result = *a;
    if (a == NULL) *result = 0;
    return 1;
  }
  else {
    return 0;
  }
}

int ptrsub(int *result, int *a, int *b)
{
  if (a == NULL && b == NULL) return 0;
  if (result != NULL) {
    *result = *a - *b;
    if (b == NULL) *result = *a;
    if (a == NULL) *result = 0 - *b;
    return 1;
  }
  else {
    return 0;
  }
}

int ptrmul(int *result, int *a, int *b)
{
  if (a == NULL && b == NULL) return 0;
  if (result != NULL) {
    *result = *a * *b;
    if (b == NULL || a == NULL) *result = 0;
    return 1;
  } 
  else {
    return 0;
  }
}

int ptrdiv(int *result, int *a, int *b)
{
  if (a == NULL && b == NULL) return 0;
  if (result != NULL && b != NULL && *b != 0) {
    if (a == NULL) *result = 0;
    else *result = *a / *b;
    return 1;
  }
  else {
    return 0;
  }
}

int main()
{
  int a = 0;
  int b = 0;
  int result = 2137;

  char mode = 0;
  char junk = 0;
  /* LDA */
  int ret = 0;
  do {
    printf("a = ");
    ret = scanf("%d", &a);
  } while (ret != 1);

  while ((junk = getchar()) != '\n' && junk != EOF) { }

  /* LDC */
  do {
    printf("oper: [+,-,*,/] ");
    mode = getchar();
  } while (mode != '+' && mode != '-' && mode != '*' && mode != '/');

  while ((junk = getchar()) != '\n' && junk != EOF) { }

  /* LDB */
  ret = 0;
  do {
    printf("b = ");
    ret = scanf("%d", &b);
  } while (ret != 1);

  switch (mode) {
  case '+':
    ret = ptradd(&result, &a, &b);
    break;
  case '-':
    ret = ptrsub(&result, &a, &b);
    break;
  case '*':
    ret = ptrmul(&result, &a, &b);
    break;
  case '/':
    ret = ptrdiv(&result, &a, &b);
    break;
  default:
    printf("wrong operation\n");
    break;
  }

  if (ret) {
    printf("= %d\n", result);
  }
  else {
    printf("Something happened.\n");
  }
  return !ret;
}

#include <stdio.h>
#include <ctype.h>

int main()
{
  char mode;
  char word[33]; /* konstantynopolitańczykowianeczka */
  int ret = 0;

  do {
    printf("Podaj wyraz do 32 znaków: ");
    ret = scanf("%s", &word);
  } while (ret != 1);

  while ((mode = getchar()) != '\n' && mode != EOF) { }
  
  printf("l - małe litery; u - wielkie litery: ");
  do {
    printf("[l,u] ");
    scanf(" %c", &mode);
  } while ((mode != 'u')
        && (mode != 'U') 
        && (mode != 'l') 
        && (mode != 'L'));
  
  for (int i = 0; *(word+i) != '\0'; i++) {
    switch (mode) {
    case 'u':
    case 'U':
      *(word+i) = toupper(*(word+i));
      break;
    case 'l':
    case 'L':
      *(word+i) = tolower(*(word+i));
      break;
    default:
      return 1;
      break;
    }
    printf("%c", *(word+i));
  }

  puts("");
  return 0;
}


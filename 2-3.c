#include <stdio.h>

int main()
{
  int intWrt00 = 0;
  int intWrt01 = 0;
  int intWrt02 = 0;
  int intWrt03 = 0;
  int intWrt04 = 0;
  int intTab[5] = {0, 0, 0, 0, 0};
  unsigned int *pInt;
  int counter = 0;
  
  pInt = &intWrt00;
  *pInt = 10+counter;
  printf("*pInt = %d, ", *pInt);
  printf("pInt = %X, ", pInt);
  printf("&pInt = %X\n", &pInt);
  pInt++;
  counter++;

  pInt = &intWrt01;
  *pInt = 10+counter;
  printf("*pInt = %d, ", *pInt);
  printf("pInt = %X, ", pInt);
  printf("&pInt = %X\n", &pInt);
  pInt++;
  counter++;

  pInt = &intWrt02;
  *pInt = 10+counter;
  printf("*pInt = %d, ", *pInt);
  printf("pInt = %X, ", pInt);
  printf("&pInt = %X\n", &pInt);
  pInt++;
  counter++;

  pInt = &intWrt03;
  *pInt = 10+counter;
  printf("*pInt = %d, ", *pInt);
  printf("pInt = %X, ", pInt);
  printf("&pInt = %X\n", &pInt);
  pInt++;
  counter++;

  pInt = &intWrt04;
  *pInt = 10+counter;
  printf("*pInt = %d, ", *pInt);
  printf("pInt = %X, ", pInt);
  printf("&pInt = %X\n", &pInt);
  pInt++;
  counter++;
  puts("");


  pInt = intTab;
  for (int i = 0; i < 5; i++) {
    *(pInt+i) = 110+i;
    printf("*(%X+%d) = (110+%d) = %d, ", pInt, i, i, *(pInt+i));
    printf("(%X+%d) = %X, ", pInt, i, (pInt+i));
    printf("&%X+%d = %X\n", pInt, i, &pInt+i);
  }

  return 0;
}

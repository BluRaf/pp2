#include <stdio.h>

int main()
{
	int a;
	int *ptr;
	
	a = 1337;
	ptr = &a;
	
	printf("%d\n", a);
	printf("%d\n", *ptr);
	printf("%d\n", &a);
	
	*ptr = 2137;
	
	printf("%d\n", a);
	printf("%d\n", *ptr);
	printf("%d\n", &a);
	
	return 0;
}

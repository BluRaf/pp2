#include <stdio.h>

int main()
{
	int a[8];
	int *ptr_a;
	ptr_a = &a[0];
	
	float b[8];
	float *ptr_b;
	ptr_b = &b[0];
	
	char c[8];
	char *ptr_c;
	ptr_c = &c[0];
	
	for (int i=0; i<8; i++) {
		*(ptr_a+i) = i+1;
		*(ptr_b+i) = 1.0*(i+1);
		*(ptr_c+i) = 64+i+1;
	}
	
	for (int j=1; j<8; j+=2) {
		printf("%d, ", *(ptr_a+j));
	}
	puts("");
	for (int j=1; j<8; j+=2) {
		printf("%f, ", *(ptr_b+j));
	}
	puts("");
	for (int j=1; j<8; j+=2) {
		printf("%c, ", *(ptr_c+j));
	}
	puts("");
	
	return 0;
}

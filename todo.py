#!/usr/bin/env python3

import csv
import math
from pathlib import Path

all_tasks = 0
with open('all.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=",")
    tasks = []

    for row in reader:
        tasks.append(row)
        all_tasks += len(row)

    csvfile.close()

section_counter = 1
not_done = 0
for section in tasks:
    for task in section:
        filepath = "{}-{}.c".format(section_counter, task)
        file = Path(filepath)
        if file.is_file() is False:
            print("{}.{}".format(section_counter, task))
            not_done += 1
    section_counter += 1
